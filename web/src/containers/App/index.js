import React, {Component} from "react";
import "./style.scss";
import Flight from "components/Flight";
import Map from "components/Map";
import {rxConnect} from "rx-connect";
import Rx from "rx";
import "rx-dom";

const event = {
    "meta": {"schema_version": "V1", "origin": "micro"},
    "flights": [{
        "number": "SU 2520",
        "status": "wait",
        "airline": {"name": "Аэрофлот", "iata": "SU", "icao": "AFL"},
        "departure": {
            "scheduled": "2017-05-27T10:05:00+03:00",
            "iata": "SVO",
            "name": "Шереметьево-E",
            "city": {"name": "Москва", "latitude": 0.0, "longitude": 0.0},
            "country": {"name": "Россия", "latitude": 0.0, "longitude": 0.0},
            "tz": "Europe/Moscow"
        },
        "arrival": {
            "scheduled": "2017-05-27T14:35:00+02:00",
            "iata": "AGP",
            "name": "Малага",
            "city": {"name": "Малага", "latitude": 0.0, "longitude": 0.0},
            "country": {"name": "Испания", "latitude": 0.0, "longitude": 0.0},
            "tz": null
        }
    }, {
        "number": "SU 2529",
        "status": "wait",
        "airline": {"name": "Аэрофлот", "iata": "SU", "icao": "AFL"},
        "departure": {
            "scheduled": "2017-06-08T23:25:00+02:00",
            "iata": "AGP",
            "name": "Малага",
            "city": {"name": "Малага", "latitude": 0.0, "longitude": 0.0},
            "country": {"name": "Испания", "latitude": 0.0, "longitude": 0.0},
            "tz": "Europe/Madrid"
        },
        "arrival": {
            "scheduled": "2017-06-09T05:35:00+03:00",
            "iata": "SVO",
            "name": "Шереметьево-E",
            "city": {"name": "Москва", "latitude": 0.0, "longitude": 0.0},
            "country": {"name": "Россия", "latitude": 0.0, "longitude": 0.0},
            "tz": null
        }
    }]
};

@rxConnect(() => {
    return Rx.DOM.fromEventSource('/sse')
        .debounce(1000 /* ms */)
        .map(event => JSON.parse(event))
        .flatMap(ticket => Rx.Observable.merge(
            Rx.Observable.of(ticket),
            Rx.Observable.of(ticket.flights)
                .flatMap(flights => Rx.Observable.from(flights)
                    .flatMap(flight => {
                        const {departure, arrival} = flight;
                        return Rx.Observable.from([
                            {
                                title: departure.city.name,
                                latitude: departure.city.latitude,
                                longitude: departure.city.longitude,
                            },
                            {
                                title: arrival.city.name,
                                latitude: arrival.city.latitude,
                                longitude: arrival.city.longitude,
                            }
                        ]);
                    })
                    .distinct()
                    .toArray()
                ).map(event => ({points: event}))
        ))
        .startWith(event);
})
export default class App extends Component {
    render() {
        const {flights, points} = this.props;

        return (
            <div className="viewport">
                <Map points={points}/>
                <div className="flights">
                    {flights.map(flight => <Flight key={flight.number} {...flight}/>)}
                </div>
            </div>
        );
    }
}
