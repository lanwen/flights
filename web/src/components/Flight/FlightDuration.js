import React from "react";
import moment from "moment";
import "moment-duration-format";

export default ({departure, arrival}) => {
    const start = moment(departure).utcOffset(departure);
    const end = moment(arrival).utcOffset(arrival);
    const duration = moment.duration(end.diff(start));

    return (
        <div className="times-duration">
            {duration.format('HH:mm')}
        </div>
    );
};
