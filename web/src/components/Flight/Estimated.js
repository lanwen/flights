import React from "react";
import moment from "moment";
import "moment-duration-format";

export default ({scheduled}) => {
    const end = moment(scheduled).utcOffset(scheduled);
    const start = moment();
    const duration = moment.duration(end.diff(start));

    return (
        <div className="estimated">
            <span className="estimated-date">{end.format('DD MMM YYYY')}</span>
            <span className="estimated-delay">{duration.format('M[m] d[d] H[h]:m[m]')}</span>
        </div>
    );
};
