import React from "react";
import moment from "moment";

export default ({scheduled}) => {
    return (
        <div className="when">
            <span className="when-time">{moment(scheduled).utcOffset(scheduled).format('HH:mm')}</span>
        </div>
    );
};
