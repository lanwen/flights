import React from "react";

export default ({iata, name}) => {
    return (
        <div className="airport">
            <span className="airport-iata">{iata}</span>
            <span className="airport-name">{name}</span>
        </div>
    );
};
