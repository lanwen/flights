import React from "react";

export default ({city, country}) => {
    return (
        <div className="point">
            <span className="point-city">{city.name}</span>
            <span className="point-country">{country.name}</span>
        </div>
    );
};
