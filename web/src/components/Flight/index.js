import React, {Component} from "react";
import Airport from "./Airport";
import FlightTime from "./FlightTime";
import FlightDuration from "./FlightDuration";
import Estimated from "./Estimated";
import Point from "./Point";
import "./style.scss";


export default class Flight extends Component {

    render() {
        const {departure, arrival} = this.props;

        return (
            <div className="flight">
                <Airport {...departure}/>
                <Airport {...arrival}/>

                <div className="times">
                    <FlightTime {...departure}/>
                    <FlightDuration departure={departure.scheduled} arrival={arrival.scheduled}/>
                    <FlightTime {...arrival}/>
                </div>

                <div className="points">
                    <Point {...departure}/>
                    <Point {...arrival}/>
                </div>

                <Estimated {...departure}/>
            </div>
        );
    }
}
