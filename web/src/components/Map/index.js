import React, {Component} from "react";
import "ammap3";
import "ammap3/ammap/maps/js/worldLow";
import AmCharts from "amcharts3-react";

import "./style.scss";
import {targetSVG} from "./SvgImages";

const datamap = {
    "map": "worldLow",
    "lines": [],
    "images": []
};

export default class Map extends Component {
    render() {
        const {points = []} = this.props;

        const latitudes = points.map(point => point.latitude);
        const longitudes = points.map(point => point.longitude);

        datamap.lines = [
            {
                "id": "line1",
                "arc": -0.85,
                "alpha": 0.3,
                "latitudes": latitudes,
                "longitudes": longitudes
            },
            {
                "id": "line2",
                "alpha": 0,
                "color": "#000000",
                "latitudes": latitudes,
                "longitudes": longitudes
            }
        ];

        const [centerLat, centerLong] = [
            latitudes.reduce(((a, b) => a + b), 0) / (latitudes.length || 1),
            longitudes.reduce(((a, b) => a + b), 0) / (longitudes.length || 1)
        ];


        datamap.images = []
        // .concat([
        //     {
        //         "svgPath": planeSVG,
        //         "positionOnLine": 0,
        //         "color": "#000000",
        //         "alpha": 0.1,
        //         "animateAlongLine": true,
        //         "lineId": "line2",
        //         "flipDirection": false,
        //         "loop": true,
        //         "scale": 0.03,
        //         "positionScale": 2.3
        //     },
        //     {
        //         "svgPath": planeSVG,
        //         "positionOnLine": 0,
        //         "color": "#585869",
        //         "animateAlongLine": true,
        //         "lineId": "line1",
        //         "flipDirection": false,
        //         "loop": true,
        //         "scale": 0.03,
        //         "positionScale": 1.3
        //     }])
        ;


        // console.log(JSON.stringify(datamap));

        return (
            <div className="map">
                <AmCharts.React
                     dataProvider={{
                         ...datamap,
                         images: points.map(point => ({...point, svgPath: targetSVG})),
                         zoomLatitude: centerLat,
                         zoomLongitude: centerLong,
                         zoomLevel: 3
                     }}
                    type="map"
                    theme="light" {...{
                    "areasSettings": {
                        "unlistedAreasOutlineColor": "#404040",
                        "unlistedAreasAlpha": 1,
                        "unlistedAreasColor": "#fff",
                        "autoZoom": true
                    },

                    "imagesSettings": {
                        "color": "#ffcc00",
                        "rollOverColor": "#585869",
                        "selectedColor": "#585869",
                        "pauseDuration": 0.2,
                        "animationDuration": 2.5,
                        "adjustAnimationSpeed": true
                    },

                    "linesSettings": {
                        "color": "#ffcc00",
                        "alpha": 0.5
                    },
                }} />
            </div>
        );
    }
}
