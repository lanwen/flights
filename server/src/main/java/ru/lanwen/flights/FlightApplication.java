package ru.lanwen.flights;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FlightApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlightApplication.class, args);
    }

    @Bean
    public Module jaxbJson() {
        return new JaxbAnnotationModule();
    }

    @Bean
    public SseBroadcaster broadcaster() {
        return new TicketBroadcaster();
    }


}
