package ru.lanwen.flights;

import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.glassfish.jersey.server.ChunkedOutput;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author lanwen (Merkushev Kirill)
 */
@Slf4j
public class TicketBroadcaster extends SseBroadcaster {

    private AtomicLong clients = new AtomicLong();

    @Override
    public boolean add(ChunkedOutput chunkedOutput) {
        log.info("client=connected\tcount={}", clients.incrementAndGet());
        return super.add(chunkedOutput);
    }

    @Override
    public void onClose(ChunkedOutput<OutboundEvent> chunkedOutput) {
        super.onClose(chunkedOutput);
        log.info("client=disconnected\tcount={}", clients.decrementAndGet());
    }
}
