package ru.lanwen.flights.api;

import org.glassfish.jersey.media.sse.SseFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.stereotype.Component;

/**
 * @author lanwen (Merkushev Kirill)
 */
@Component
public class ApiConfig extends ResourceConfig {
    public ApiConfig() {
        register(SseFeature.class);
        register(TicketResource.class);
        property(ServletProperties.FILTER_FORWARD_ON_404, true);
    }
}
