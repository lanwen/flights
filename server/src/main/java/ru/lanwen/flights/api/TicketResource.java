package ru.lanwen.flights.api;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.glassfish.jersey.media.sse.SseFeature;
import org.springframework.stereotype.Component;
import ru.lanwen.heathrow.beans.Eticket;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author lanwen (Merkushev Kirill)
 */
@Component
@Path("/")
public class TicketResource {

    @Inject
    private SseBroadcaster broadcaster;

    @POST
    @Path("ticket")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Eticket ticket(Eticket eticket) {
        broadcaster.broadcast(
                new OutboundEvent.Builder()
                        .data(eticket)
                        .mediaType(MediaType.APPLICATION_JSON_TYPE)
                        .build()
        );

        return eticket;
    }

    @GET
    @Path("sse")
    @Produces(SseFeature.SERVER_SENT_EVENTS)
    public EventOutput sse() {
        final EventOutput eventOutput = new EventOutput();
        this.broadcaster.add(eventOutput);
        return eventOutput;
    }
}
