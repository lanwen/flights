package ru.lanwen.flights;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.sse.EventSource;
import org.glassfish.jersey.media.sse.InboundEvent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.lanwen.heathrow.beans.Eticket;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 * @author lanwen (Merkushev Kirill)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = FlightApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "management.port=-1"
)
@EnableAutoConfiguration
public class FlightApplicationTest {

    @Value("http://localhost:${local.server.port}/")
    private String uri;

    @Inject
    private Client client;

    private List<InboundEvent> events = new LinkedList<>();

    @Rule
    public ExternalResource closer = new ExternalResource() {
        private EventSource eventSource;
        private CompletableFuture<Void> open;

        @Override
        protected void before() throws Throwable {
            eventSource = EventSource.target(client.target(uri).path("sse")).build();
            eventSource.register(events::add);

            open = CompletableFuture.runAsync(() -> eventSource.open());
        }

        @Override
        protected void after() {
            open.cancel(true);
            eventSource.close();
        }
    };

    @Test
    public void shouldSendAndParse() throws Exception {
        Eticket ticket = client.target(uri).path("ticket")
                .request()
                .post(Entity.entity(cp("tickets/ticket1.json"), MediaType.APPLICATION_JSON_TYPE))
                .readEntity(Eticket.class);

        assertThat(ticket.getFlights(), hasSize(6));
        assertThat("tz", ticket.getFlights().get(0).getDeparture().getScheduled().getZone(), is(ZoneId.of("+03:00")));
    }

    @Test
    public void shouldSse() throws Exception {
        client.target(uri).path("ticket")
                .request()
                .post(Entity.entity(cp("tickets/ticket1.json"), MediaType.APPLICATION_JSON_TYPE))
                .readEntity(Eticket.class);

        assertThat(events, hasSize(1));
        assertThat(events.get(0).readData(Eticket.class).getFlights(), hasSize(6));
    }

    @TestConfiguration
    public static class Conf {

        @Bean
        public Client client() {
            return ClientBuilder.newClient()
                    .register(
                            new LoggingFeature(
                                    Logger.getLogger(FlightApplicationTest.class.getCanonicalName()),
                                    Level.INFO,
                                    LoggingFeature.Verbosity.PAYLOAD_TEXT, 1024 * 100
                            )
                    );
        }
    }

    public static String cp(String path) {
        try (InputStream is = new ClassPathResource(path).getInputStream()) {
            return IOUtils.toString(is, UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
